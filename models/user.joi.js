const Joi = require('joi');

const phones = Joi.object({
  ddd: Joi.string()
    .required()
    .regex(/^0\d{2}$/),
  phone: Joi.string()
    .required()
    .regex(/^[0-9]{8,11}$/),
});

const user = Joi.object({
  name: Joi.string()
    .required()
    .regex(/^[a-zA-Z\s.]{2,80}$/),
  email: Joi.string()
    .required()
    .email({ minDomainSegments: 2, tlds: { allow: ['com', 'net'] } }),
  password: Joi.string()
    .required()
    .regex(/^[a-zA-Z0-9?!@#$%¨&*()_-]{8,20}$/),
  phones: Joi.array()
    .items(phones)
    .optional(),
}).meta({
  className: 'user',
});

module.exports = user;
