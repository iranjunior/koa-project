const Koa = require('koa');
const { ui, validate } = require('swagger2-koa');
const swaggerJSDoc = require('swagger-jsdoc');
const koaBody = require('koa-body');
const bodyParser = require('koa-bodyparser');
const swaggerDefinition = require('./config/swaggerDefinitions');
const database = require('./config/database');
const compileSwagger = require('./utils/swaggerJson');

const router = require('./api/routers');

const app = new Koa();

const options = {
  swaggerDefinition,
  apis: ['./api/**/*.js'],
};

const swaggerSpec = compileSwagger(swaggerJSDoc(options));

database.connect();
app.use(
  koaBody({
    strict: false,
  }),
);
app.use(
  bodyParser({
    enableTypes: ['json'],
    jsonLimit: '2mb',
    strict: true,
    onerror(err, ctx) {
      ctx.throw('body parse error', 422);
    },
  }),
);
app.use(ui(swaggerSpec, '/swagger'));
app.use(validate(swaggerSpec));
app.use(router.routes());
app.use(router.allowedMethods());

module.exports = app;
