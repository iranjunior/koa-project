const request = require('supertest');
const { truncate } = require('../utils/truncate');
const factory = require('../utils/factory');
const app = require('../../server');
const User = require('../../models/user');

const supertest = request(app.callback());

describe('Teste de criação de usuario', () => {
  beforeAll(async () => {
    await truncate(User);
  });

  afterAll(async () => {
    await truncate(User);
  });

  it('deve falhar devido a informaçoes estarem erradas', async () => {
    const user = await factory.build('user', { email: undefined });
    const response = await supertest.post('/user').send(user);

    expect(response.body.code).toBe('SWAGGER_REQUEST_VALIDATION_FAILED');
  });

  it('deve criar usuario com sucesso', async () => {
    const user = await factory.build('user', {
      email: 'iranjunior94@gmail.com',
    });

    const response = await supertest.post('/user').send(user);

    expect(response.body.uuid).toMatch(/^[a-zA-Z0-9]{22}$/);
  });

  it('deve falhar devido ao usuario já existir', async () => {
    const user = await factory.build('user', {
      email: 'iranjunior94@gmail.com',
    });
    const response = await supertest.post('/user').send(user);

    expect(response.body.message).toBe('Usuario já cadastrado');
  });
});
