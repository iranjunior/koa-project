const RandExp = require('randexp');

const userExist = {
  checkUser: () => new Promise((resolve) => setTimeout(resolve(true), 100)),
};
const userDontExist = {
  checkUser: () => new Promise((resolve) => setTimeout(resolve(true), 100)),
};
const createUserWithSuccess = {
  create: () => new Promise((resolve) => setTimeout(
    resolve({
      uuid: new RandExp(/^[a-zA-Z0-9]{22}$/).gen(),
      lastLogin: new Date().toISOString(),
      createdAt: new Date().toISOString(),
      updatedAt: new Date().toISOString(),
      token: new RandExp(/^[a-zA-Z0-9._-]{130}$/),
    }),
    100,
  )),
};

const dontCreateUser = {
  create: () => new Promise((resolve, reject) => setTimeout(reject(new Error('Falha ao criar')), 100)),
};

module.exports = {
  userExist: () => ({ ...userExist }),
  userDontExist: () => ({ ...userDontExist }),
  createSuccess: () => ({ ...userDontExist, ...createUserWithSuccess }),
  createFailed: () => ({ ...userDontExist, ...dontCreateUser }),
};
