const FactoryGirl = require('factory-girl');

const { factory } = FactoryGirl;
const faker = require('faker');
const User = require('../../models/user');

factory.define('user', User, {
  name: faker.name.findName(),
  email: faker.internet.email(),
  password: faker.internet.password(),
  phones: [
    {
      ddd: '081',
      phone: '97907717',
    },
    {
      ddd: '081',
      phone: '97901317',
    },
  ],
});
/*
async function promise() {
  const user = await factory.create('user', {});
  return user;
}
 */

module.exports = factory;
