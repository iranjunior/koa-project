const app = require('./server');
const { port } = require('./config/vars');

app.listen(port);
