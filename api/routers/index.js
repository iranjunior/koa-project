const Router = require('koa-router');
const user = require('./user');

const router = new Router();
router.use(user.routes());
/**
 * @swagger
 * /:
 *   get:
 *     description: Returns the homepage
 *     tags:
 *       - Hello
 *     responses:
 *       200:
 *         description: hello world
 */
router.get('/', (ctx) => {
  ctx.body = 'Hello World';
});

module.exports = router;
