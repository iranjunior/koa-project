const Router = require('koa-router');
const UserModel = require('../../../models/user');

const router = new Router();
/**
 * @swagger
 * /user:
 *   post:
 *     description: Create User
 *     tags:
 *          - User
 *     parameters:
 *          -  name: CreateUser
 *             required: true
 *             in: body
 *             schema:
 *               $ref: '#/definitions/user'
 *     responses:
 *      200:
 *         description: Retorna informações do usuario
 *         schema:
 *           type: object
 *      403:
 *         description: Retorna informações do usuario
 *         schema:
 *           type: object
 *
 */


router.post('/user', async (ctx) => {
  const { email } = ctx.request.body;
  const exist = await UserModel.checkUser(email);

  if (exist) {
    ctx.body = {
      message: 'Usuario já cadastrado',
    };
    ctx.status = 403;
  } else {
    const {
      uuid,
      lastLogin,
      createdAt,
      updatedAt,
      token,
    } = await UserModel.create(ctx.request.body);

    ctx.body = {
      uuid,
      lastLogin,
      createdAt,
      updatedAt,
      token,
    };
  }
});

module.exports = router;
