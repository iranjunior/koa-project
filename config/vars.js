const dotenv = require('dotenv');
const path = require('path');

dotenv.config({
  path: path.resolve(__dirname, '..', '.env'),
  sample: path.resolve(__dirname, '..', '.envDefault'),
});

module.exports = {
  env: process.env.NODE_ENV || 'development',
  host: process.env.HOST,
  port: process.env.PORT,
  secret: process.env.SECRET,
  expiredTime: process.env.EXPIRED_TIME,
  dbURl:
        process.env.NODE_ENV === 'test'
          ? process.env.DB_URI_TEST
          : process.env.DB_URI,
};
