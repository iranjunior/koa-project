const mongoose = require('mongoose');
const { dbURl, env } = require('../config/vars');

module.exports.connect = async () => {
  if (env !== 'production') mongoose.set('debug', true);
  else mongoose.set('debug', false);

  await mongoose.connect(dbURl, {
    useNewUrlParser: true,
    keepAlive: true,
    useCreateIndex: true,
  });

  return mongoose.connection;
};
