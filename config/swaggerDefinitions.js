const { host, port } = require('./vars');

module.exports = {
  info: {
    title: 'Api KOA',
    version: '1.0.0',
    description: 'Api rest com koa',
  },
  host: `${host}:${port}`,
  basePath: '/',
};
